package eu.reactunion.lf7.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import eu.reactunion.lf7.constants.HelloPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Roman Srom (roman.srom@lundegaard.eu)
 */
@Component
public class HelloPortletConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return HelloPortletConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return HelloPortletKeys.HELLO;
    }
}
