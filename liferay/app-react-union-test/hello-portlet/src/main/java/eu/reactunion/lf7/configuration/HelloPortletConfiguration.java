package eu.reactunion.lf7.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import eu.reactunion.lf7.constants.HelloPortletKeys;

/**
 * Configuration class of the Hero portlet.
 *
 * @author Roman Srom (roman.srom@lundegaard.eu)
 */
@ExtendedObjectClassDefinition(
        category = "foundation",
        scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(id = HelloPortletKeys.CONFIGURATION)
public interface HelloPortletConfiguration {

    @Meta.AD(
            required = true,
            deflt = ""
    )
    String heading();

    @Meta.AD(
            required = true,
            deflt = ""
    )
    String content();

}
