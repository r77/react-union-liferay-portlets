package eu.reactunion.lf7.constants;

/**
 * Portlet Keys
 *
 * @author Roman Srom (roman.srom@lundegaard.eu)
 */
public class HelloPortletKeys {

	public static final String HELLO = "eu_reactunion_lf7_portlet_Hello";
	public static final String CONFIGURATION = "eu.reactunion.lf7.configuration.HelloPortletConfiguration";

}
