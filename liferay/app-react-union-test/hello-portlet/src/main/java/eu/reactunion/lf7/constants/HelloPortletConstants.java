package eu.reactunion.lf7.constants;

/**
 * Hero widget's constants.
 *
 * @author Roman Srom (roman.srom@lundegaard.eu)
 */
public class HelloPortletConstants {

    public static final String ATTR_HEADING = "heading";
    public static final String ATTR_CONTENT = "content";

    public static final String PARAM_HEADING = "heading";
    public static final String PARAM_CONTENT = "content";

    public static final String ATTR_PORTAL_USER = "portalUser";
    public static final String ATTR_PORTAL_USER_SCREENNAME = "portalUserScreenName";

}
