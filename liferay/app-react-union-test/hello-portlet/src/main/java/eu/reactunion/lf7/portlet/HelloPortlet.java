package eu.reactunion.lf7.portlet;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;
import eu.reactunion.lf7.configuration.HelloPortletConfigurationUtil;
import eu.reactunion.lf7.constants.HelloPortletKeys;
import org.osgi.service.component.annotations.Component;

import static eu.reactunion.lf7.constants.HelloPortletConstants.ATTR_PORTAL_USER_SCREENNAME;

/**
 * Controller of Hero Portlet, it renders view.jsp with the React widgets.
 *
 * @author Roman Srom (roman.srom@lundegaard.eu)
 */
@Component(
        configurationPid = HelloPortletKeys.CONFIGURATION,
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.sample",
                "com.liferay.portlet.instanceable=true",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + HelloPortletKeys.HELLO,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user"
        },
        service = Portlet.class
)
public class HelloPortlet extends MVCPortlet {



    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        try {
            HelloPortletConfigurationUtil.addConfigurationContext(renderRequest);
            User portalUser = PortalUtil.getUser(renderRequest);
            renderRequest.setAttribute(ATTR_PORTAL_USER_SCREENNAME, portalUser == null ? "ANONYM" : portalUser.getScreenName());
        } catch (ConfigurationException e) {
            throw new PortletException("Configuration error", e);
        } catch (PortalException e) {
            throw new PortletException("Portal error", e);
        }

        super.render(renderRequest, renderResponse);
    }

}
