<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>
<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
  <script type="text/javascript">
	Liferay.Loader.require("app-t-mobile-widgets");
</script>
<div id="${ns}hello"></div>
<script data-union-widget="hello" data-union-container="${ns}hello" type="application/json">
    {"user": {"name":"<%=renderRequest.getAttribute("portalUserScreenName")%>"}}
</script>

