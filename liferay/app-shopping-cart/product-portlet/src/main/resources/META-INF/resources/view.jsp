<%@ include file="init.jsp" %>
<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
    Liferay.Loader.require("app-shopping-cart");
</script>
<div id="${ns}product-list"></div>


<script data-union-widget="product-list" data-union-container="${ns}product-list" type="application/json">
{"user": {"name":"<%=user.getScreenName()%>"}}</script>
