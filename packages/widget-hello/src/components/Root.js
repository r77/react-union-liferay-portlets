import React from 'react';
import PropTypes from 'prop-types';

import logo from './logo.png';

const Root = ({data: {user}}) => (
	<section>
		<img src={logo} />
		<p>Bud zdráv uživateli s jménem : {user.name}</p>
	</section>
);


Root.defaultProps = {
	data: {user: {name: 'anonym'}},
};

Root.propTypes = {
	data: PropTypes.shape({
		user: PropTypes.shape({
			name: PropTypes.string,
		}),
	}),
};

export default Root;
