import loadHello from 'widget-hello';

export default [
	{
		path: 'hello',
		getComponent(done) {
			loadHello(mod => done(mod.default));
		},
	},
];
