import Product from './components/Product';
import Products from './components/Products';

export { Product, Products };
