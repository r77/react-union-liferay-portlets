import React, { Component } from 'react';
import PropTypes from 'prop-types';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const Counter = (function(_Component) {
	_inherits(Counter, _Component);

	function Counter(props) {
		_classCallCheck(this, Counter);

		const _this = _possibleConstructorReturn(
			this,
			(Counter.__proto__ || Object.getPrototypeOf(Counter)).call(this, props)
		);

		_this.state = { value: _this.props.productQuantity };
		_this.increment = _this.increment.bind(_this);
		_this.decrement = _this.decrement.bind(_this);
		return _this;
	}

	_createClass(Counter, [
		{
			key: 'increment',
			value: function increment(e) {
				this.setState(
					function(prevState) {
						return {
							value: Number(prevState.value) + 1,
						};
					},
					function() {
						this.props.updateQuantity(this.state.value);
					}
				);
				e.preventDefault();
			},
		},
		{
			key: 'decrement',
			value: function decrement(e) {
				e.preventDefault();
				if (this.state.value <= 1) {
					return this.state.value;
				} else {
					this.setState(
						function(prevState) {
							return {
								value: Number(prevState.value) - 1,
							};
						},
						function() {
							this.props.updateQuantity(this.state.value);
						}
					);
				}
			},
		},
		{
			key: 'feed',
			value: function feed(e) {
				this.setState(
					{
						value: this.refs.feedQty.value,
					},
					function() {
						this.props.updateQuantity(this.state.value);
					}
				);
			},
		},
		{
			key: 'resetQuantity',
			value: function resetQuantity() {
				this.setState({
					value: 1,
				});
			},
		},
		{
			key: 'render',
			value: function render() {
				return React.createElement(
					'div',
					{ className: 'stepper-input' },
					React.createElement(
						'a',
						{ href: '#', className: 'decrement', onClick: this.decrement },
						'\u2013'
					),
					React.createElement('input', {
						ref: 'feedQty',
						type: 'number',
						className: 'quantity',
						value: this.state.value,
						onChange: this.feed.bind(this),
					}),
					React.createElement(
						'a',
						{ href: '#', className: 'increment', onClick: this.increment },
						'+'
					)
				);
			},
		},
	]);

	return Counter;
})(Component);

Counter.propTypes = {
	value: PropTypes.number,
};

export default Counter;
