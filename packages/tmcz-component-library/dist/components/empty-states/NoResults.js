import React, { Component } from 'react';

const NoResults = function NoResults() {
	return React.createElement(
		'div',
		{ className: 'products' },
		React.createElement(
			'div',
			{ className: 'no-results' },
			React.createElement('img', {
				src: 'https://res.cloudinary.com/sivadass/image/upload/v1494699523/icons/bare-tree.png',
				alt: 'Empty Tree',
			}),
			React.createElement('h2', null, 'Sorry, no products matched your search!'),
			React.createElement('p', null, 'Enter a different keyword and try.')
		)
	);
};

export default NoResults;
