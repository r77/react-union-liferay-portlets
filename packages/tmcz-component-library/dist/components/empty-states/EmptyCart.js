import React, { Component } from 'react';

const EmptyCart = function EmptyCart(props) {
	return React.createElement(
		'div',
		{ className: 'empty-cart' },
		React.createElement('img', {
			src: 'https://res.cloudinary.com/sivadass/image/upload/v1495427934/icons/empty-cart.png',
			alt: 'empty-cart',
		}),
		React.createElement('h2', null, 'You cart is empty!')
	);
};

export default EmptyCart;
