import React, { Component } from 'react';

const Footer = function Footer(props) {
	return React.createElement(
		'footer',
		null,
		React.createElement(
			'p',
			{ className: 'footer-links' },
			React.createElement(
				'a',
				{
					href: 'https://github.com/sivadass/react-shopping-cart',
					target: '_blank',
				},
				'View Source on Github'
			),
			React.createElement('span', null, ' / '),
			React.createElement(
				'a',
				{ href: 'mailto:contact@sivadass.in', target: '_blank' },
				'Need any help?'
			),
			React.createElement('span', null, ' / '),
			React.createElement(
				'a',
				{ href: 'https://twitter.com/NSivadass', target: '_blank' },
				'Say Hi on Twitter'
			),
			React.createElement('span', null, ' / '),
			React.createElement('a', { href: 'https://sivadass.in', target: '_blank' }, 'Read My Blog')
		),
		React.createElement(
			'p',
			null,
			'\xA9 2017 ',
			React.createElement('strong', null, 'Veggy'),
			' - Organic Green Store'
		)
	);
};

export default Footer;
