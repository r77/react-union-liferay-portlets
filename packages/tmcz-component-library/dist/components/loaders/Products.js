import React, { Component } from 'react';
import Product from './Product';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const LoadingProducts = (function(_Component) {
	_inherits(LoadingProducts, _Component);

	function LoadingProducts() {
		_classCallCheck(this, LoadingProducts);

		return _possibleConstructorReturn(
			this,
			(LoadingProducts.__proto__ || Object.getPrototypeOf(LoadingProducts)).apply(this, arguments)
		);
	}

	_createClass(LoadingProducts, [
		{
			key: 'render',
			value: function render() {
				return React.createElement(
					'div',
					{ className: 'products loading' },
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null),
					React.createElement(Product, null)
				);
			},
		},
	]);

	return LoadingProducts;
})(Component);

export default LoadingProducts;
