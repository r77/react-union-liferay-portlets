import React, { Component } from 'react';

const LoadingProduct = function LoadingProduct() {
	return React.createElement(
		'div',
		{ className: 'product loading' },
		React.createElement('div', { className: 'product-image' }),
		React.createElement('div', { className: 'product-text' }),
		React.createElement('div', { className: 'product-text' }),
		React.createElement('div', { className: 'product-text' }),
		React.createElement('div', { className: 'product-button' })
	);
};

export default LoadingProduct;
