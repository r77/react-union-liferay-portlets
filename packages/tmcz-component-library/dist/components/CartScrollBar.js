import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const CartScrollBar = (function(_Component) {
	_inherits(CartScrollBar, _Component);

	function CartScrollBar(props) {
		_classCallCheck(this, CartScrollBar);

		const _this = _possibleConstructorReturn(
			this,
			(CartScrollBar.__proto__ || Object.getPrototypeOf(CartScrollBar)).call(this, props)
		);

		_this.handleScroll = _this.handleScroll.bind(_this);
		return _this;
	}

	_createClass(CartScrollBar, [
		{
			key: 'componentDidMount',
			value: function componentDidMount() {
				window.addEventListener('scroll', this.handleScroll);
			},
		},
		{
			key: 'componentWillUnmount',
			value: function componentWillUnmount() {
				window.removeEventListener('scroll', this.handleScroll);
			},
		},
		{
			key: 'handleScroll',
			value: function handleScroll(event) {
				const positions = this.refs.scrollbars.getValues();
				// When the bottom is reached and we're scrolling down, prevent scrolling of the window
				if (positions.top >= 1) {
					console.log('Reached scroll end!');
					event.stopPropagation();
				}
			},
		},
		{
			key: 'render',
			value: function render() {
				return React.createElement(
					Scrollbars,
					{ style: { width: 360, height: 320 }, ref: 'scrollbars' },
					this.props.children
				);
			},
		},
	]);

	return CartScrollBar;
})(Component);

export default CartScrollBar;
