import React, { Component } from 'react';
import CartScrollBar from './CartScrollBar';
import Counter from './Counter';
import EmptyCart from './empty-states/EmptyCart';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { findDOMNode } from 'react-dom';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const Header = (function(_Component) {
	_inherits(Header, _Component);

	function Header(props) {
		_classCallCheck(this, Header);

		const _this = _possibleConstructorReturn(
			this,
			(Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props)
		);

		_this.state = {
			showCart: false,
			cart: _this.props.cartItems,
			mobileSearch: false,
		};
		return _this;
	}

	_createClass(Header, [
		{
			key: 'handleCart',
			value: function handleCart(e) {
				e.preventDefault();
				this.setState({
					showCart: !this.state.showCart,
				});
			},
		},
		{
			key: 'handleSubmit',
			value: function handleSubmit(e) {
				e.preventDefault();
			},
		},
		{
			key: 'handleMobileSearch',
			value: function handleMobileSearch(e) {
				e.preventDefault();
				this.setState({
					mobileSearch: true,
				});
			},
		},
		{
			key: 'handleSearchNav',
			value: function handleSearchNav(e) {
				e.preventDefault();
				this.setState(
					{
						mobileSearch: false,
					},
					function() {
						this.refs.searchBox.value = '';
						this.props.handleMobileSearch();
					}
				);
			},
		},
		{
			key: 'handleClickOutside',
			value: function handleClickOutside(event) {
				const cartNode = findDOMNode(this.refs.cartPreview);
				const buttonNode = findDOMNode(this.refs.cartButton);
				if (cartNode.classList.contains('active')) {
					if (!cartNode || !cartNode.contains(event.target)) {
						this.setState({
							showCart: false,
						});
						event.stopPropagation();
					}
				}
			},
		},
		{
			key: 'componentDidMount',
			value: function componentDidMount() {
				document.addEventListener('click', this.handleClickOutside.bind(this), true);
			},
		},
		{
			key: 'componentWillUnmount',
			value: function componentWillUnmount() {
				document.removeEventListener('click', this.handleClickOutside.bind(this), true);
			},
		},
		{
			key: 'render',
			value: function render() {
				const _this2 = this;

				let cartItems = void 0;
				cartItems = this.state.cart.map(function(product) {
					return React.createElement(
						'li',
						{ className: 'cart-item', key: product.name },
						React.createElement('img', { className: 'product-image', src: product.image }),
						React.createElement(
							'div',
							{ className: 'product-info' },
							React.createElement('p', { className: 'product-name' }, product.name),
							React.createElement('p', { className: 'product-price' }, product.price)
						),
						React.createElement(
							'div',
							{ className: 'product-total' },
							React.createElement(
								'p',
								{ className: 'quantity' },
								product.quantity,
								' ',
								product.quantity > 1 ? 'Nos.' : 'No.',
								' '
							),
							React.createElement('p', { className: 'amount' }, product.quantity * product.price)
						),
						React.createElement(
							'a',
							{
								className: 'product-remove',
								href: '#',
								onClick: _this2.props.removeProduct.bind(_this2, product.id),
							},
							'\xD7'
						)
					);
				});
				let view = void 0;
				if (cartItems.length <= 0) {
					view = React.createElement(EmptyCart, null);
				} else {
					view = React.createElement(
						CSSTransitionGroup,
						{
							transitionName: 'fadeIn',
							transitionEnterTimeout: 500,
							transitionLeaveTimeout: 300,
							component: 'ul',
							className: 'cart-items',
						},
						cartItems
					);
				}
				return React.createElement(
					'header',
					null,
					React.createElement(
						'div',
						{ className: 'container' },
						React.createElement(
							'div',
							{ className: 'brand' },
							React.createElement('img', {
								className: 'logo',
								src:
									'https://res.cloudinary.com/sivadass/image/upload/v1493547373/dummy-logo/Veggy.png',
								alt: 'Veggy Brand Logo',
							})
						),
						React.createElement(
							'div',
							{ className: 'search' },
							React.createElement(
								'a',
								{
									className: 'mobile-search',
									href: '#',
									onClick: this.handleMobileSearch.bind(this),
								},
								React.createElement('img', {
									src:
										'https://res.cloudinary.com/sivadass/image/upload/v1494756966/icons/search-green.png',
									alt: 'search',
								})
							),
							React.createElement(
								'form',
								{
									action: '#',
									method: 'get',
									className: this.state.mobileSearch ? 'search-form active' : 'search-form',
								},
								React.createElement(
									'a',
									{
										className: 'back-button',
										href: '#',
										onClick: this.handleSearchNav.bind(this),
									},
									React.createElement('img', {
										src:
											'https://res.cloudinary.com/sivadass/image/upload/v1494756030/icons/back.png',
										alt: 'back',
									})
								),
								React.createElement('input', {
									type: 'search',
									ref: 'searchBox',
									placeholder: 'Search for Vegetables and Fruits',
									className: 'search-keyword',
									onChange: this.props.handleSearch,
								}),
								React.createElement('button', {
									className: 'search-button',
									type: 'submit',
									onClick: this.handleSubmit.bind(this),
								})
							)
						),
						React.createElement(
							'div',
							{ className: 'cart' },
							React.createElement(
								'div',
								{ className: 'cart-info' },
								React.createElement(
									'table',
									null,
									React.createElement(
										'tbody',
										null,
										React.createElement(
											'tr',
											null,
											React.createElement('td', null, 'No. of items'),
											React.createElement('td', null, ':'),
											React.createElement(
												'td',
												null,
												React.createElement('strong', null, this.props.totalItems)
											)
										),
										React.createElement(
											'tr',
											null,
											React.createElement('td', null, 'Sub Total'),
											React.createElement('td', null, ':'),
											React.createElement(
												'td',
												null,
												React.createElement('strong', null, this.props.total)
											)
										)
									)
								)
							),
							React.createElement(
								'a',
								{
									className: 'cart-icon',
									href: '#',
									onClick: this.handleCart.bind(this),
									ref: 'cartButton',
								},
								React.createElement('img', {
									className: this.props.cartBounce ? 'tada' : ' ',
									src: 'https://res.cloudinary.com/sivadass/image/upload/v1493548928/icons/bag.png',
									alt: 'Cart',
								}),
								this.props.totalItems
									? React.createElement('span', { className: 'cart-count' }, this.props.totalItems)
									: ''
							),
							React.createElement(
								'div',
								{
									className: this.state.showCart ? 'cart-preview active' : 'cart-preview',
									ref: 'cartPreview',
								},
								React.createElement(CartScrollBar, null, view),
								React.createElement(
									'div',
									{ className: 'action-block' },
									React.createElement(
										'button',
										{
											type: 'button',
											className: this.state.cart.length > 0 ? ' ' : 'disabled',
										},
										'PROCEED TO CHECKOUT'
									)
								)
							)
						)
					)
				);
			},
		},
	]);

	return Header;
})(Component);

export default Header;
