import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const QuickView = (function(_Component) {
	_inherits(QuickView, _Component);

	function QuickView(props) {
		_classCallCheck(this, QuickView);

		return _possibleConstructorReturn(
			this,
			(QuickView.__proto__ || Object.getPrototypeOf(QuickView)).call(this, props)
		);
	}

	_createClass(QuickView, [
		{
			key: 'componentDidMount',
			value: function componentDidMount() {
				document.addEventListener('click', this.handleClickOutside.bind(this), true);
			},
		},
		{
			key: 'componentWillUnmount',
			value: function componentWillUnmount() {
				document.removeEventListener('click', this.handleClickOutside.bind(this), true);
			},
		},
		{
			key: 'handleClickOutside',
			value: function handleClickOutside(event) {
				const domNode = findDOMNode(this.refs.modal);
				if (!domNode || !domNode.contains(event.target)) {
					this.props.closeModal();
				}
			},
		},
		{
			key: 'handleClose',
			value: function handleClose() {
				this.props.closeModal();
			},
		},
		{
			key: 'render',
			value: function render() {
				return React.createElement(
					'div',
					{
						className: this.props.openModal ? 'modal-wrapper active' : 'modal-wrapper',
					},
					React.createElement(
						'div',
						{ className: 'modal', ref: 'modal' },
						React.createElement(
							'button',
							{
								type: 'button',
								className: 'close',
								onClick: this.handleClose.bind(this),
							},
							'\xD7'
						),
						React.createElement(
							'div',
							{ className: 'quick-view' },
							React.createElement(
								'div',
								{ className: 'quick-view-image' },
								React.createElement('img', {
									src: this.props.product.image,
									alt: this.props.product.name,
								})
							),
							React.createElement(
								'div',
								{ className: 'quick-view-details' },
								React.createElement('span', { className: 'product-name' }, this.props.product.name),
								React.createElement(
									'span',
									{ className: 'product-price' },
									this.props.product.price
								)
							)
						)
					)
				);
			},
		},
	]);

	return QuickView;
})(Component);

export default QuickView;
