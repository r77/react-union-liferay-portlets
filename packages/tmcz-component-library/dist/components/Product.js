import React, { Component } from 'react';
import Counter from './Counter';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const Product = (function(_Component) {
	_inherits(Product, _Component);

	function Product(props) {
		_classCallCheck(this, Product);

		const _this = _possibleConstructorReturn(
			this,
			(Product.__proto__ || Object.getPrototypeOf(Product)).call(this, props)
		);

		_this.state = {
			selectedProduct: {},
			quickViewProdcut: {},
			isAdded: false,
		};
		return _this;
	}

	_createClass(Product, [
		{
			key: 'addToCart',
			value: function addToCart(image, name, price, id, quantity) {
				this.setState(
					{
						selectedProduct: {
							image,
							name,
							price,
							id,
							quantity,
						},
					},
					function() {
						this.props.addToCart(this.state.selectedProduct);
					}
				);
				this.setState(
					{
						isAdded: true,
					},
					function() {
						const _this2 = this;

						setTimeout(function() {
							_this2.setState({
								isAdded: false,
								selectedProduct: {},
							});
						}, 3500);
					}
				);
			},
		},
		{
			key: 'quickView',
			value: function quickView(image, name, price, id) {
				this.setState(
					{
						quickViewProdcut: {
							image,
							name,
							price,
							id,
						},
					},
					function() {
						this.props.openModal(this.state.quickViewProdcut);
					}
				);
			},
		},
		{
			key: 'render',
			value: function render() {
				const image = this.props.image;
				const name = this.props.name;
				const price = this.props.price;
				const id = this.props.id;
				const quantity = this.props.productQuantity;
				return React.createElement(
					'div',
					{ className: 'product' },
					React.createElement(
						'div',
						{ className: 'product-image' },
						React.createElement('img', {
							src: image,
							alt: this.props.name,
							onClick: this.quickView.bind(this, image, name, price, id, quantity),
						})
					),
					React.createElement('h4', { className: 'product-name' }, this.props.name),
					React.createElement('p', { className: 'product-price' }, this.props.price),
					React.createElement(Counter, {
						productQuantity: quantity,
						updateQuantity: this.props.updateQuantity,
						resetQuantity: this.resetQuantity,
					}),
					React.createElement(
						'div',
						{ className: 'product-action' },
						React.createElement(
							'button',
							{
								className: !this.state.isAdded ? '' : 'added',
								type: 'button',
								onClick: this.addToCart.bind(this, image, name, price, id, quantity),
							},
							!this.state.isAdded ? 'ADD TO CART' : '✔ ADDED'
						)
					)
				);
			},
		},
	]);

	return Product;
})(Component);

export default Product;
