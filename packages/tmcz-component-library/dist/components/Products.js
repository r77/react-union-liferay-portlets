import React, { Component } from 'react';
import Product from './Product';
import LoadingProducts from './loaders/Products';
import NoResults from './empty-states/NoResults';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const _createClass = (function() {
	function defineProperties(target, props) {
		for (let i = 0; i < props.length; i++) {
			const descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function(Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);
		if (staticProps) defineProperties(Constructor, staticProps);
		return Constructor;
	};
})();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function');
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== 'function' && superClass !== null) {
		throw new TypeError(
			`Super expression must either be null or a function, not ${ typeof superClass}`
		);
	}
	subClass.prototype = Object.create(superClass && superClass.prototype, {
		constructor: { value: subClass, enumerable: false, writable: true, configurable: true },
	});
	if (superClass)
		Object.setPrototypeOf
			? Object.setPrototypeOf(subClass, superClass)
			: (subClass.__proto__ = superClass);
}

const Products = (function(_Component) {
	_inherits(Products, _Component);

	function Products() {
		_classCallCheck(this, Products);

		return _possibleConstructorReturn(
			this,
			(Products.__proto__ || Object.getPrototypeOf(Products)).call(this)
		);
	}

	_createClass(Products, [
		{
			key: 'render',
			value: function render() {
				const _this2 = this;

				let productsData = void 0;
				const term = this.props.searchTerm;
				const x = void 0;

				function searchingFor(term) {
					return function(x) {
						return x.name.toLowerCase().includes(term.toLowerCase()) || !term;
					};
				}
				productsData = this.props.productsList.filter(searchingFor(term)).map(function(product) {
					return React.createElement(Product, {
						key: product.id,
						price: product.price,
						name: product.name,
						image: product.image,
						id: product.id,
						addToCart: _this2.props.addToCart,
						productQuantity: _this2.props.productQuantity,
						updateQuantity: _this2.props.updateQuantity,
						openModal: _this2.props.openModal,
					});
				});

				// Empty and Loading States
				let view = void 0;
				if (productsData.length <= 0 && !term) {
					view = React.createElement(LoadingProducts, null);
				} else if (productsData.length <= 0 && term) {
					view = React.createElement(NoResults, null);
				} else {
					view = React.createElement(
						CSSTransitionGroup,
						{
							transitionName: 'fadeIn',
							transitionEnterTimeout: 500,
							transitionLeaveTimeout: 300,
							component: 'div',
							className: 'products',
						},
						productsData
					);
				}
				return React.createElement('div', { className: 'products-wrapper' }, view);
			},
		},
	]);

	return Products;
})(Component);

export default Products;
