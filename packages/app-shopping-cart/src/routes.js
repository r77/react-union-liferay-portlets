import loadProductList from 'widget-product-list';
import loadProductFilter from 'widget-product-filter';

export default [
	{
		path: 'product-list',
		getComponent(done) {
			loadProductList(mod => done(mod.default));
		},
	},
	{
		path: 'product-filter',
		getComponent(done) {
			loadProductFilter(mod => done(mod.default));
		},
	},
];
