import React from 'react';
import PropTypes from 'prop-types';

const Root = ({ data: { user } }) => (
	<section>
		Nabídka pro uživatele {user.name}
		<ul>
			<li>
				<img src="https://www.t-mobile.cz/dcpublic/1f34ffa7-26ee-4603-ac20-f2925d4fc9b4.jpg" />
			</li>
			<li>
				<img src="https://www.t-mobile.cz/dcpublic/iPhone_XS_stribrny-listing.jpg" />
			</li>
		</ul>
	</section>
);

Root.defaultProps = {
	data: { user: { name: 'anonym' } },
};

Root.propTypes = {
	data: PropTypes.shape({
		user: PropTypes.shape({
			name: PropTypes.string,
		}),
	}),
};

export default Root;
