import React, { Component } from 'react';

class ProductFilterDialog extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFiltered: false,
		};
	}

	doFilter() {
		this.setState({ isFiltered: !this.state.isFiltered });
	}

	render() {
		return (
			<section>
				<button
					className={!this.state.isFiltered ? '' : 'added'}
					type="button"
					onClick={this.doFilter.bind(this)}
				>
					{!this.state.isFiltered ? 'Filtruj' : '✔ Zafiltrovano'}
				</button>
			</section>
		);
	}
}

export default ProductFilterDialog;
