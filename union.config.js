module.exports = ({ target }) => ({
	workspaces: {
		widgetPattern: ['widget'],
		appPattern: ['app'],
	},
	sourceMaps: false,
	...(target === 'liferay'
		? {
				outputMapper: {
					// move JS form react-union to `widgets` folder in Liferay theme
					js: 'js',
				},
				apps: [
					{
						name: 'app-shopping-cart',
						// setup your public path of your Liferay AMD loader
						publicPath: '/o/liferay-amd-loader/app-shopping-cart/',
						proxy: {
							// setup the URL of your locally running Liferay
							target: 'http://localhost:8080',
							// setup public path of your Liferay AMD loader
							publicPath: '/o/liferay-amd-loader/app-shopping-cart/',
						},
					},
					{
						name: 'app-react-union-test',
						// setup your public path of your Liferay AMD loader
						publicPath: '/o/liferay-amd-loader/app-react-union-test/',
						proxy: {
							// setup the URL of your locally running Liferay
							target: 'http://localhost:8080',
							// setup public path of your Liferay AMD loader
							publicPath: '/o/liferay-amd-loader/app-react-union-test/',
						},
					},
				],
		  }
		: {}),
});
